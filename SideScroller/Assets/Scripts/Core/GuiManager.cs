﻿using UnityEngine;
using System.Collections;

public class GuiManager : MonoBehaviour, IGuiManager {

	public enum GuiScreen{ StartMenu, GameOver, InGame};

	public StartMenuScreen startMenuScreen;
	private GameObject currentScreen;

	void Awake()
	{
		currentScreen = null;
	}

	// Use this for initialization
	void Start () {
		ChangeGuiScreen((int)GuiScreen.StartMenu);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeGuiScreen(int screen)
	{
		if(currentScreen)
			currentScreen.SetActive(false);
		switch(screen)
		{
		case (int)GuiScreen.StartMenu:

			startMenuScreen.gameObject.SetActive(true);
			currentScreen = startMenuScreen.gameObject;
			break;
		case (int)GuiScreen.GameOver:
			
			break;
		case (int)GuiScreen.InGame:
			
			break;
		}
	}
}
