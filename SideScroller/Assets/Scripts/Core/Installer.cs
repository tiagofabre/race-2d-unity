﻿using UnityEngine;
using System.Collections;
using Zenject;

public class Installer : MonoInstaller
{
    public override void InstallBindings()
    {
		Container.Bind<IGameManager>().ToSingle<GameManager>();
		Container.Bind<IGameController>().ToSingle<GameController>();

		InstantiateGui();
		Container.Bind<IGuiManager>().ToInstance((GuiManager)MonoBehaviour.FindObjectOfType<GuiManager>());

    }

	//ainda se adaptando ao zenject
	//TODO reavaliar esse metodo de pegar instancia
	private void InstantiateGui()
	{
		GameObject x = MonoBehaviour.Instantiate(Resources.Load("GuiManager")) as GameObject;
		x.transform.parent = transform;
	}
}
