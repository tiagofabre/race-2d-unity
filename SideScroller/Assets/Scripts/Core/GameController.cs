﻿using UnityEngine;
using System.Collections;
using Zenject;

public class GameController : IGameController {

	[Inject]
	IGameManager gameManager;
	
	public GameController(IGameManager _gameManager)
	{
		gameManager = _gameManager;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartGame()
	{

	}
}
