﻿using UnityEngine;
using System.Collections;
using Zenject;
public class GameManager : IGameManager
{
	[Inject]
	IGuiManager guiManager;
    
	public enum GameStates { WaitingToStart, Playing, GameOver }

    public GameStates gameState;

	public GameManager(IGuiManager _guiManager)
	{
		guiManager = _guiManager;
	}

	void Awake () {
        gameState = GameStates.WaitingToStart;
	}

	public void SetGameState(GameStates state)
	{
		gameState = state;
	}

    public void StartScripts()
    {
		SetGameState(GameManager.GameStates.Playing);
		guiManager.ChangeGuiScreen((int)GuiManager.GuiScreen.InGame);
		Application.LoadLevelAdditive(1);
    }
}
