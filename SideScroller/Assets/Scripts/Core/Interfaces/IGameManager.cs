﻿using System.Collections;

public interface IGameManager
{
    void StartScripts();
	void SetGameState(GameManager.GameStates state);
}
