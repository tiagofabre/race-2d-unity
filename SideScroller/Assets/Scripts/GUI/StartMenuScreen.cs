﻿using UnityEngine;
using System.Collections;
using Zenject;

public class StartMenuScreen : MonoBehaviour {

	[Inject]
	IGameManager game;

	public StartMenuScreen(IGameManager _game)
	{
		game = _game;
	}
	public void StartButton()
	{
		game.StartScripts();
	}
}
