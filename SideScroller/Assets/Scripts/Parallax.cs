﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public GameObject reference;
	public float percent;
	private Vector3 lastPosition;

	// Use this for initialization
	void Start () {
		lastPosition = reference.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		lastPosition = reference.transform.position;
		transform.position = new Vector3(lastPosition.x, lastPosition.y  * percent);
	
	}
}
